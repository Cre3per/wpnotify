const web = require('./web.js');
const Config = require('./config.js');

const exec = require('child_process').exec;
const http = require('http');


class WPNotify
{
  filterName(str)
  {
    const whitelist = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ';

    let result = '';

    for (let c of str)
    {
      if (whitelist.includes(c))
      {
        result += c;
      }
    }

    return result;
  }

  localizeDate(date)
  {
    const HOUR_OFFSET = 12;
    const fix2 = (str) => { return (str.length == 2 ? str : ('0' + str)); }

    let localDate = new Date(date);

    localDate.setHours(localDate.getHours() + HOUR_OFFSET);

    let year = localDate.getUTCFullYear().toString();
    let month = fix2((localDate.getUTCMonth() + 1).toString());
    let day = fix2(localDate.getUTCDate().toString());
    let hour = fix2(localDate.getUTCHours().toString());
    let minute = fix2(localDate.getUTCMinutes().toString());

    return `${day}.${month}.${year} ${hour}:${minute}`;
  }

  _onNewComment(comment)
  {
    if (comment.author_name === 'nascardriver')
    {
      if (comment.parent && this._config.unchecked.includes(comment.parent))
      {
        this._markCommentRead(comment.parent);
      }

      return;
    }

    let filteredName = this.filterName(comment.author_name);

    exec(`notify-send \"learncpp\" \"${filteredName}\"`);

    if (!this._config.unchecked.includes(comment.id))
    {
      this._config.unchecked.push(comment.id);
    }

    this._pendingComments.push(comment);

    this._config.write();
  }

  _getCommentLessonName(comment)
  {
    const DIGITS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

    let rawStart = (comment.link.indexOf('ial/') + 4);

    let split = comment.link.substr(rawStart).split('-');

    for (let word of split)
    {
      if (word.includes('/'))
      {
        break;
      }

      for (let digit of DIGITS)
      {
        if (word.includes(digit))
        {
          rawStart += (word.length + 1);
          break;
        }
      }
    }

    let rawEnd = comment.link.indexOf('/', rawStart);

    let rawName = comment.link.substring(rawStart, rawEnd);

    rawName = rawName.split('-').join(' ');

    split = rawName.split(' ');
    rawName = '';

    for (let word of split)
    {
      if (word.startsWith('std'))
      {
        word = `std::${word.substr(3)}`;
      }

      rawName += ' ' + word;
    }

    let displayName = rawName.substr(1);

    return displayName;
  }

  _printComment(comment)
  {
    const MIN_NAME_LENGTH = 16;

    let authorName = comment.author_name;
    let lessonName = this._getCommentLessonName(comment);
    let url = comment.link;
    let markReadUrl = `http://localhost:${this._port}/${comment.id}`;

    while (authorName.length < MIN_NAME_LENGTH)
    {
      authorName += ' ';
    }

    let mention = comment.content.rendered.toLowerCase().includes('nascardriver');

    console.log(`[${this.localizeDate(comment.date)}]${mention ? ' @nascardriver' : ''}`);
    console.log(authorName);
    console.log(`> ${lessonName}`);
    console.log(`> ${url}`);
    console.log(`> Mark read ${markReadUrl}`);
  }

  _refreshScreen()
  {
    process.stdout.write('\x1Bc');

    if (this._pendingComments.length > 0)
    {
      // Loop inverse to have old comments at the bottom
      for (let i = (this._pendingComments.length - 1); i >= 0; --i)
      {
        let comment = this._pendingComments[i];

        this._printComment(comment);
        console.log('');
      }
    }
    else
    {
      console.log('All caught up');
    }
  }

  _getCommentsAfterDate(callback)
  {
    let url = `https://www.learncpp.com/wp-json/wp/v2/comments?per_page=100&after=${this._config.lastCheck}`;
    web.getJSON(url, undefined, callback);
  }

  _getCommentsById(ids, callback)
  {
    let url = `https://www.learncpp.com/wp-json/wp/v2/comments?per_page=100&include=`;

    for (let i = 0; i < ids.length; ++i)
    {
      let id = ids[i];

      url += (id + ((i === (ids.length - 1)) ? '' : ','));
    }

    web.getJSON(url, undefined, callback);
  }

  _getLeftOverComments(callback)
  {
    this._getCommentsById(this._config.unchecked, callback);
  }

  _getCommentsSinceLastCheck(callback)
  {
    this._getCommentsAfterDate(callback);
  }

  run()
  {
    const commentHandler = (err, data) =>
    {
      if (err)
      {
        console.log(`Error getting data:`);
        console.log(err);
      }
      else if (data.code)
      {
        console.log(`Error getting data:`);
        console.log(data);
      }
      else
      {
        if (data.length > 0)
        {
          this._config.lastCheck = data[0].date;

          for (let i = (data.length - 1); i >= 0; --i)
          {
            let comment = data[i];

            this._onNewComment(comment);
          }

          this._refreshScreen();
        }
      }
    }

    if (this._isFirstRequest)
    {
      this._isFirstRequest = false;
      this._getLeftOverComments(commentHandler);
    }

    this._getCommentsSinceLastCheck(commentHandler);

    if (this._config.interval >= 5000)
    {
      setTimeout(() => { this.run.apply(this); }, this._config.interval);
    }
    else
    {
      console.log(`No config interval (${this._config.interval})! Aborting.`);
    }
  }

  _getPendingCommentById(id)
  {
    for (let comment of this._pendingComments)
    {
      if (comment.id === id)
      {
        return comment;
      }
    }

    return undefined;
  }

  _markCommentRead(id)
  {
    for (let i = 0; i < this._pendingComments.length; ++i)
    {
      let comment = this._pendingComments[i];

      if (comment.id === id)
      {
        this._pendingComments.splice(i, 1);
        break;
      }
    }

    for (let i = 0; i < this._config.unchecked.length; ++i)
    {
      if (this._config.unchecked[i] === id)
      {
        this._config.unchecked.splice(i, 1);
        break;
      }
    }

    this._config.write();

    this._refreshScreen();
  }

  _onClick(id)
  {
    console.log(`Requested id ${id}`);

    let comment = this._getPendingCommentById(id);

    if (comment)
    {
      this._markCommentRead(id);

      return comment.link;
    }
    else
    {
      console.log(`Requested non-existing comment: ${id}`);

      return undefined;
    }
  }

  _createServer(callback)
  {
    this._sv = http.createServer((req, res) =>
    {
      let redirect = this._onClick(parseInt(req.url.substr(1)));

      if (false && redirect)
      {
        res.writeHead(302, { 'location': redirect });
      }
      else
      {
        res.writeHead(404);
      }

      res.write('<html><script>window.close();</script></html>');

      res.end();
    });

    this._sv.listen(this._port, (err) =>
    {
      if (err)
      {
        console.log('Could not create server');
        console.log(err);
      }
      else
      {
        callback();
      }
    });
  }

  constructor()
  {
    this._isFirstRequest = true;

    this._port = 1887;

    this._pendingComments = [];

    this._sv = undefined;

    this._config = new Config(`${__dirname}/config.json`);

    this._createServer(() =>
    {
      console.log(`Server is running on port ${this._port}`);
      console.log('');

      this._config.read(() =>
      {
        this.run();
      });
    });
  }
}

new WPNotify();