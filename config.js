const fs = require('fs');

class Config
{
  read(callback)
  {
    fs.readFile(this._configPath, 'utf8', (err, data) =>
    {
      if (err)
      {
        console.log(`Error reading config:`);
        console.log(err);
      }
      else
      {
        try
        {
          Object.assign(this, JSON.parse(data));
        }
        catch (ex)
        {

        }
      }

      callback && callback();
    });
  }

  write(callback)
  {
    this._deferredSave = JSON.stringify({ interval: this.interval, lastCheck: this.lastCheck, unchecked: this.unchecked });

    if (!this._deferredWrite)
    {
      this._deferredWrite = setTimeout(() =>
      {
        this._deferredWrite = null;

        fs.writeFile(this._configPath, this._deferredSave, (err) =>
        {
          if (err)
          {
            console.log(`Error saving config:`);
            console.log(err);
          }

          callback && callback();
        });
      }, 1000);
    }
  }

  constructor(configPath)
  {
    this._configPath = configPath;
  }
}

module.exports = Config;